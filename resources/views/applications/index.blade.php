@extends('layouts.app')

@section('content')

  
  <!-- Dynamic Content -->
  <div class="container-fluid"> 
    <div class="tab-content clearfix">
      <div class="container-fluid h3">Application List</div>
      <hr class="content-hr">
      <table class="table table-responsive table-hover table-border-outline">
        <thead>
          <tr>
            <form method="POST">
              <th class="w-15" scope="col">
                <div class="input-group">
                  <select class="form-control required" id="inputAppliType" name="inputAppliType">
                    @if(isset($formAppliTypes) && is_array($formAppliTypes))
                    @foreach ($formAppliTypes as $key => $val)
                      <option value={{ $key }}>{{ $val }}</option>
                    @endforeach
                    @endif
                  </select>
                </div>
              </th>
              <th class="w-25" scope="col">
                <div class="input-group">
                  <input class="form-control py-2 border-right-0 border" name="employeeName"  type="text" placeholder="Employee Name" autocomplete="name">
                </div>
              </th>
              <th class="w-20 center" scope="col">Duration</th>
              <th class="w-20" scope="col">
                <div class="input-group">
                  <input id="datePicker" type="text" class="form-control" placeholder="Date Applied" data-provide="datepicker" data-date-orientation="bottom" data-date-format="yyyy/mm/dd">
                  <span class="input-group-append">
                    <div class="input-group-text bg-transparent"><i class="fa fa-fw fa-calendar"></i></div>
                  </span>
                </div>
              </th>
              <th class="w-15" scope="col">
                <div class="input-group">
                  <input class="form-control py-2 border-right-0 border" name="applicationStatus"  type="text" placeholder="Status">
                </div>
              </th>
              <th class="w-15 center" scope="col">Action</th>
              <th scope="col">
                <button class="btn btn-outline-secondary" type="button">
                    <i class="fa fa-search"></i>
                </button>
              </th>
            </form>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="w-15">Sick Leave</td>
            <td class="w-25">Juan Dela Cruz</td>
            <td class="w-20 center">2018/07/05 - 2018/07/05</td>
            <td class="w-20 center">2018/07/15</td>
            <td class="w-15 center">Pending</td>
            <td class="center" colspan="2">
              <a class="btn btn-outline-dark" href="/applications/view"><span><i class="fa fa-info-circle"></i></span></a>
              <a class="btn btn-outline-dark" href="/applications/edit"><span><i class="fa fa-edit"></i></span></a>
            </td>
          </tr>
        </tbody>
      </table>
      <div class="container-fluid paginator d-flex">
        <button class="btn btn-outline-dark btn-prev mr-auto"> < Previous </button>
        <button class="btn btn-outline-dark btn-next ml-auto"> > Next </button>
      </div>
    </div>
  </div>
@endsection