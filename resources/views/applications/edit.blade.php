@extends('layouts.app')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/applications.css') }}">
@endpush

@section('content')

  
  <!-- Dynamic Content -->
  <div class="container"> 
    <div class="tab-content clearfix">
      <div class="container h3"><i class="fa fa-info-circle smarts-color-red"></i> Edit Application</div>
      <hr class="content-hr">
      <div class="row">
        <div class="col-md-7 application-col">
          <form method="POST" action="{{ route('applications.edit') }}" id="applicationForm">
            {{ csrf_field() }}
            <!-- Employee Name -->
            <div class="form-group row">
              <label for="inputEmployeeName" class="col-sm-4 col-form-label">Employee Name</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="inputEmployeeName" name="inputEmployeeName" placeholder="Employee Name" value="Juan Dela Cruz">
              </div>
            </div>
            <!-- Segregator Code -->
            <div class="form-group row">
              <label for="inputSegregatorCode" class="col-sm-4 col-form-label">Segregator Code</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="inputSegregatorCode" name="inputSegregatorCode" placeholder="Segregator Code" value="15">
              </div>
            </div>
            <hr>
            <!-- Application Type -->
            <div class="form-group row">
              <label for="inputAppliType" class="col-sm-4 col-form-label">Application Type</label>
              <div class="col-sm-8 input-group">
                <select class="form-control required" id="inputAppliType" name="inputAppliType">
                  @if(isset($formAppliTypes) && is_array($formAppliTypes))
                  @foreach ($formAppliTypes as $key => $val)
                    <option value={{ $key }} {{ $key == 4 ? 'selected' : '' }}>{{ $val }}</option>
                  @endforeach
                  @endif
                </select>
              </div>
            </div>
            <!-- Start Date -->
            <div class="form-group row">
              <label for="inputStartDate" class="col-sm-4 col-form-label">Start Date</label>
              <div class="col-sm-8 input-group">
                <input type="text" class="form-control" id="inputStartDate" name="inputStartDate" placeholder="2018/07/10" data-provide="datepicker" data-date-orientation="bottom" data-date-format="yyyy/mm/dd">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
            <!-- End Date -->
            <div class="form-group row">
              <label for="inputEndDate" class="col-sm-4 col-form-label">End Date</label>
              <div class="col-sm-8 input-group">
                <input type="text" class="form-control" id="inputEndDate" name="inputEndDate" placeholder="2018/07/11" data-provide="datepicker" data-date-orientation="bottom" data-date-format="yyyy/mm/dd">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
            <!--  -->
            <div class="form-group row">
              <label for="inputDay" class="col-sm-4 col-form-label"></label>
              <div class="col-sm-8 input-group">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked>
                  <label class="form-check-label" for="inlineRadio1">Whole Day</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                  <label class="form-check-label" for="inlineRadio2">Half Day</label>
                </div>
              </div>
            </div>
            <!-- Duration -->
            <div class="form-group row">
              <label for="inputDuration" class="col-sm-4 col-form-label">Duration</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="inputDuration" name="inputDuration" placeholder="Duration" value="2 day">
              </div>
            </div>
            <!-- Reason -->
            <div class="form-group row">
              <label for="inputReason" class="col-sm-4 col-form-label">Reason</label>
              <div class="col-sm-8">
                <textarea class="form-control" id="inputReason" name="inputReason" rows="5">I need a BREAK~</textarea>
              </div>
            </div>
            <button class="btn" style="display: hidden;" type="submit" id="applicationFormSubmit"></button>
          </form>
        </div>
        <!-- Actions -->
        <div class="col-md-5 d-flex align-items-start flex-column">
          <div class="row w-100">
            <div class="offset-2">
              <label>Status</label>
              <br>
              <label class="h3" style="color: orange">Pending!</label>
            </div>
          </div>
          <div class="mt-auto row w-100">
            <div class="col-md-3 offset-2 d-flex align-items-end justify-content-start">
              <a href="/applications">Back</a>
            </div>
            <div class="col-md-5 mt-auto">
              <div>
                <button class="btn w-100" id="updateButton">Update</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
<script type="text/javascript">
  $('#updateButton').on('click', function() {
    $(this).attr("disabled", "disabled");
    document.getElementById("applicationForm").submit();
  });
</script>
@endpush