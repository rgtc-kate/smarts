@extends('layouts.app')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/applications.css') }}">
@endpush

@section('content')

  
  <!-- Dynamic Content -->
  <div class="container-fluid"> 
    <div class="tab-content clearfix">
      <div class="container-fluid h3"><i class="fa fa-info-circle smarts-color-red"></i> View Application</div>
      <hr class="content-hr">
      <div class="row">
        <div class="col-md-7 application-col">
          <!-- Employee Name -->
          <div class="form-group row">
            <label for="inputEmployeeName" class="col-sm-4 col-form-label">Employee Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmployeeName" placeholder="Employee Name" value="Juan Dela Cruz" readonly>
            </div>
          </div>
          <!-- Segregator Code -->
          <div class="form-group row">
            <label for="inputSegregatorCode" class="col-sm-4 col-form-label">Segregator Code</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputSegregatorCode" placeholder="Segregator Code" value="15" readonly>
            </div>
          </div>
          <hr>
          <!-- Application Type -->
          <div class="form-group row">
            <label for="inputAppliType" class="col-sm-4 col-form-label">Application Type</label>
            <div class="col-sm-8 input-group">
              <select class="form-control required" id="inputAppliType" name="inputAppliType" readonly disabled>
                @if(isset($formAppliTypes) && is_array($formAppliTypes))
                @foreach ($formAppliTypes as $key => $val)
                  <option value={{ $key }} {{ $key == 4 ? 'selected' : '' }}>{{ $val }}</option>
                @endforeach
                @endif
              </select>
            </div>
          </div>
          <!-- Start Date -->
          <div class="form-group row">
            <label for="inputStartDate" class="col-sm-4 col-form-label">Start Date</label>
            <div class="col-sm-8 input-group">
              <input type="text" class="form-control" id="inputStartDate" placeholder="2018/07/10" readonly>
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
              </div>
            </div>
          </div>
          <!-- End Date -->
          <div class="form-group row">
            <label for="inputEndDate" class="col-sm-4 col-form-label">End Date</label>
            <div class="col-sm-8 input-group">
              <input type="text" class="form-control" id="inputEndDate" placeholder="2018/07/11" readonly>
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
              </div>
            </div>
          </div>
          <!--  -->
          <div class="form-group row">
            <label for="inputDay" class="col-sm-4 col-form-label"></label>
            <div class="col-sm-8 input-group">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" disabled checked>
                <label class="form-check-label" for="inlineRadio1">Whole Day</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" disabled>
                <label class="form-check-label" for="inlineRadio2">Half Day</label>
              </div>
            </div>
          </div>
          <!-- Duration -->
          <div class="form-group row">
            <label for="inputDuration" class="col-sm-4 col-form-label">Duration</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputDuration" placeholder="Duration" value="2 day" readonly>
            </div>
          </div>
          <!-- Reason -->
          <div class="form-group row">
            <label for="inputReason" class="col-sm-4 col-form-label">Reason</label>
            <div class="col-sm-8">
              <textarea class="form-control" id="inputReason" rows="5" readonly>I need a BREAK~</textarea>
            </div>
          </div>
        </div>
        <!-- Actions -->
        <div class="col-md-5 d-flex align-items-start flex-column">
          <div class="row w-100">
            <div class="offset-2">
              <label>Status</label>
              <br>
              <label class="h3" id="applicationStatus" style="color: orange">Pending!</label>
            </div>
          </div>
          <div class="mt-auto row w-100">
            <div class="col-md-3 offset-2 d-flex align-items-end justify-content-start">
              <a href="/applications">Back</a>
            </div>
            <div class="col-md-5">
              <form method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                  <button class="btn w-100" type="submit">Approve</button>
                </div>
              </form>
              <form method="POST">
                {{ csrf_field() }}
                <div>
                  <button class="btn w-100">Revoke</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection