@extends('layouts.app')

@section('content')

  
  <!-- Dynamic Content -->
  <div class="container-fluid"> 
    <div class="tab-content clearfix">
      <div class="container-fluid h3"><i class="fa fa-user-plus smarts-color-red"></i> Add New User</div>
      <hr class="content-hr">
      <div class="container-fluid">
        <div class="row d-flex justify-content-center">
          <div class="col-md-7">
            <form method="POST" action="{{ route('userManage.add') }}">
              {{ csrf_field() }}
              <div class="form-group row">
                <label for="userName" class="col-sm-3 col-form-label">Username</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="userName" name="inputUserName" placeholder="juandlc" autocomplete="email">
                </div>
              </div>
              <div class="form-group row">
                <label for="employeeNo" class="col-sm-3 col-form-label">Employee No.</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="employeeNo" name="inputEmployeeNo" placeholder="20150515">
                </div>
              </div>
              <div class="form-group row">
                <label for="employeeName" class="col-sm-3 col-form-label">Employee Name</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="employeeName" name="inputEmployeeName" placeholder="Juan Dela Cruz">
                </div>
              </div>
              <div class="form-group row">
                <label for="employeePass" class="col-sm-3 col-form-label">Password</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="employeePass" name="inputEmployeePass" autocomplete>
                </div>
              </div>
              <div class="form-group row">
                <label for="employeePassConfirm" class="col-sm-3 col-form-label">Confirm Password</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="employeePassConfirm" name="inputEmployeePassConfirm" autocomplete>
                </div>
              </div>
              <div class="form-group row">
                <label for="segregatorCode" class="col-sm-3 col-form-label">Segregator Code</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="segregatorCode" name="inputSegregatorCode" placeholder="20150515">
                </div>
              </div>
              <div class="form-group row">
                <label for="role" class="col-sm-3 col-form-label">Role</label>
                <div class="col-sm-9 input-group">
                  <select class="form-control" id="inputRole" name="inputRole">
                    @if(isset($formRoles) && is_array($formRoles))
                    @foreach ($formRoles as $key => $val)
                      <option value={{ $key }} {{ $key == 1 ? 'selected' : '' }}>{{ $val }}</option>
                    @endforeach
                    @endif
                  </select>
                </div>
              </div>
              <div class="form"-group row>
                  <button class="btn col-sm-9 btn-outline-dark offset-3">Add User</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="container-fluid paginator d-flex">
        <a class="mr-auto" href="/user-manage"> Back </button>
      </div>
    </div>
  </div>
@endsection