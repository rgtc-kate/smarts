<!-- Subview Navigation Tab -->
<div class="container-fluid h3"><i class="fa fa-{{ $editScreen ? 'edit' : 'user' }} smarts-color-red"></i> {{ ucfirst($screenType) }} User</div>
<hr class="content-hr">
<div class="container-fluid user-manage-nav">
  <ul class="nav nav-pills">
    <li class="nav-item">
      <a class="nav-link {{ strpos(Route::currentRouteName(), 'personal') !== false ? 'active' : '' }}" href="/user-manage/personal/{{ $screenType }}">
        <span>Personal</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ strpos(Route::currentRouteName(), 'location') !== false ? 'active' : '' }}" href="/user-manage/location/{{ $screenType }}">
        <span>Location</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ strpos(Route::currentRouteName(), 'workSchedule') !== false ? 'active' : '' }}" href="/user-manage/work-schedule/{{ $screenType }}">
        <span>Work Schedule</span>
      </a>
    </li>
  </ul>
</div>