@extends('layouts.app')

@section('content')

  
  <!-- Dynamic Content -->
  <div class="container-fluid"> 
    <div class="tab-content clearfix">
      <div class="container-fluid h3">User Management</div>
      <hr class="content-hr">
      <div class="container-fluid">
        <div class="row">
          <a class="btn btn-light ml-auto h6" href="/user-manage/add"><i class="fa fa-user-plus"></i> ADD NEW</a>
        </div>
      </div>
      <table class="table table-responsive table-hover table-border-outline">
        <thead>
          <tr>
            <form method="POST">
              <th class="w-15" scope="col">
                <div class="input-group">
                  <input class="form-control py-2 border-right-0 border" name="inputEmployeeNo"  type="text" placeholder="Employee No.">
                </div>
              </th>
              <th class="w-30" scope="col">
                <div class="input-group">
                  <input class="form-control py-2 border-right-0 border" name="inputEmployeeName"  type="text" placeholder="Employee Name">
                </div>
              </th>
              <th class="w-25" scope="col">
                <div>
                  <select class="form-control" id="inputLocation" name="inputLocation" autocomplete="location">
                    @if(isset($formLocations) && is_array($formLocations))
                    @foreach ($formLocations as $key => $val)
                      <option value={{ $key }}>{{ $val }}</option>
                    @endforeach
                    @endif
                  </select>
                </div>  
              </th>
              <th class="w-20 center" scope="col">Duration</th>
              <th class="w-15 center" scope="col">Action</th>
              <th scope="col">
                <button class="btn btn-outline-secondary" type="button">
                    <i class="fa fa-search"></i>
                </button>
              </th>
            </form>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="w-15">20150515</td>
            <td class="w-30">Juan Dela Cruz</td>
            <td class="w-25">GMA/ Mandaluyong City</td>
            <td class="w-20">2018/01/01 - 2018/12/31</td>
            <td class="w-15" colspan="2">
              <a class="btn btn-outline-dark" href="/user-manage/personal/view"><span><i class="fa fa-info-circle"></i></span></a>
              <a class="btn btn-outline-dark" href="/user-manage/personal/edit"><span><i class="fa fa-edit"></i></span></a>
              <button class="btn btn-outline-dark" data-toggle="modal" data-target="#deleteUserModal"><span><i class="fa fa-trash"></i></span></button>
            </td>
          </tr>
        </tbody>
      </table>
      <div class="container-fluid paginator d-flex">
        <button class="btn btn-outline-dark btn-prev mr-auto"> < Previous </button>
        <button class="btn btn-outline-dark btn-next ml-auto"> > Next </button>
      </div>
    </div>
  </div>

@include('userManage.modals')
@endsection