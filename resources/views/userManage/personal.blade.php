@extends('layouts.app')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/user-manage.css') }}">
@endpush

@section('content')
@php
  $editScreen = isset($editScreen) ? $editScreen : false;
  $screenType = $editScreen ? 'edit' : 'view';
  $isReadonly = $editScreen ? '' : 'readonly';
@endphp
  
  <!-- Dynamic Content -->
  <div class="container-fluid"> 
    <div class="tab-content clearfix">
      <!-- Subview Navigation Tab -->
      @include('userManage.subNav')

      <!-- Dynamic Subcontent -->
      <div class="container-fluid">
        <div class="row d-flex justify-content-center">
          <div class="col-md-7">
            <form method="POST">
              <div class="form-group row">
                <label for="userName" class="col-sm-3 col-form-label">Username</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="userName" name="inputUserName" value="juandlc" autocomplete="email" {{ $isReadonly }}>
                </div>
                @if($editScreen)
                <span class="required"></span>
                @endif
              </div>
              <div class="form-group row">
                <label for="employeeNo" class="col-sm-3 col-form-label">Employee No.</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="employeeNo" name="inputEmployeeNo" value="20150515" {{ $isReadonly }}>
                </div>
                @if($editScreen)
                <span class="required"></span>
                @endif
              </div>
              <div class="form-group row">
                <label for="employeeName" class="col-sm-3 col-form-label">Employee Name</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="employeeName" name="inputEmployeeName" value="Juan Dela Cruz" {{ $isReadonly }}>
                </div>
                @if($editScreen)
                <span class="required"></span>
                @endif
              </div>
              <div class="form-group row">
                <label for="employeePass" class="col-sm-3 col-form-label">Password</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="employeePass" name="inputEmployeePass" value="admin123" autocomplete {{ $isReadonly }}>
                </div>
                @if($editScreen)
                <span class="required"></span>
                @endif
              </div>
              <div class="form-group row">
                <label for="employeePassConfirm" class="col-sm-3 col-form-label">Confirm Password</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="employeePassConfirm" name="inputEmployeePassConfirm" value="admin123" autocomplete {{ $isReadonly }}>
                </div>
              </div>
              <div class="form-group row">
                <label for="segregatorCode" class="col-sm-3 col-form-label">Segregator Code</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="segregatorCode" name="inputSegregatorCode" value="15" {{ $isReadonly }}>
                </div>
                @if($editScreen)
                <span class="required"></span>
                @endif
              </div>
              <div class="form-group row">
                <label for="role" class="col-sm-3 col-form-label">Role</label>
                <div class="col-sm-9 input-group">
                    <select class="form-control" id="inputRole" name="inputRole" {{ $editScreen ? '' : 'readonly disabled' }}>
                      @if(isset($formRoles) && is_array($formRoles))
                      @foreach ($formRoles as $key => $val)
                        <option value={{ $key }} {{ $key == 1 ? 'selected' : '' }}>{{ $val }}</option>
                      @endforeach
                      @endif
                    </select>
                </div>
                @if($editScreen)
                <span class="required"></span>
                @endif
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="container-fluid paginator d-flex">
        <a class="mr-auto" href="/user-manage"> Back </button>
      </div>
    </div>
  </div>
@endsection