@extends('layouts.app')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/user-manage.css') }}">
@endpush

@section('content')
@php
  $editScreen = isset($editScreen) ? $editScreen : false;
  $screenType = $editScreen ? 'edit' : 'view';
@endphp
  
  <!-- Dynamic Content -->
  <div class="container-fluid"> 
    <div class="tab-content clearfix">
      <!-- Subview Navigation Tab -->
      @include('userManage.subNav')

      <!-- Dynamic Subcontent -->
      <div class="container-fluid">
        <!-- Form -->
        <div class="row d-flex">
          <div class="col-md-7">
            <form method="POST" id="addLocationForm">
              <div class="form-group row">
                <label for="location" class="col-sm-3 col-form-label">Location</label>
                <div class="col-sm-8 input-group">
                    <select class="form-control" id="inputLocation" name="inputLocation">
                      @if(isset($formLocations) && is_array($formLocations))
                      @foreach ($formLocations as $key => $val)
                        <option value={{ $key }}>{{ $val }}</option>
                      @endforeach
                      @endif
                    </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputStartDate" class="col-sm-3 col-form-label">Start Date</label>
                <div class="col-sm-8 input-group">
                  <input type="text" class="form-control" id="inputStartDate" name="inputStartDate" placeholder="YYYY/MM/DD" data-provide="datepicker" data-date-orientation="bottom" data-date-format="yyyy/mm/dd">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEndDate" class="col-sm-3 col-form-label">End Date</label>
                <div class="col-sm-8 input-group">
                  <input type="text" class="form-control" id="inputEndDate" name="inputEndDate" placeholder="YYYY/MM/DD" data-provide="datepicker" data-date-orientation="bottom" data-date-format="yyyy/mm/dd">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </form>
          </div>
          @if(!$editScreen)
          <div class="col-md-2 offset-1 mt-auto">
            <div class="form-group row">
              <button class="btn btn-outline-dark w-80 submit-btn">ADD</button>
            </div>
          </div>
          @endif
        </div>

        <!-- Table -->
        <div class="row">
          <div class="col-md-10">
            <table class="table table-responsive table-hover table-border-outline">
              <thead>
                <tr>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Location</th>
                  @if($editScreen)
                  <th>Action</th>
                  @endif
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>2018/01/01</td>
                  <td>2018/02/28</td>
                  <td>GMA/Mandaluyong</td>
                  @if($editScreen)
                  <td>
                    <button class="btn btn-outline-dark"><span><i class="fa fa-edit"></i></span></button>
                    <button class="btn btn-outline-dark"><span><i class="fa fa-close"></i></span></button>
                  </td>
                  @endif
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="container-fluid paginator d-flex">
        <a class="mr-auto" href="/user-manage"> Back </button>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
<script type="text/javascript">
  $('.submit-btn').on('click', function() {
    $(this).attr("disabled", "disabled");

    // -- ajax request
  });
</script>
@endpush