@extends('layouts.app')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/home.css') }}">
@endpush

@section('content')

  
  <!-- Dynamic Content -->
  <div class="container-fluid"> 
    <div class="tab-content clearfix">
      <div class="container-fluid h3">Realtime List</div>
      <hr class="content-hr">
      <table class="table table-responsive table-hover table-border-outline">
        <thead>
          <tr>
            <form method="POST">
              <th class="w-30" scope="col">
                <div class="input-group">
                  <span class="input-group-prepend">
                    <div class="input-group-text bg-transparent"><i class="fa fa-fw fa-user-circle"></i></div>
                  </span>
                  <input class="form-control py-2 border-right-0 border" name="inputemployeeName" type="text" placeholder="Employee Name" autocomplete="name">
                </div>
              </th>
              <th class="w-25" scope="col">
                <div>
                  <select class="form-control" id="inputLocation" name="inputLocation" autocomplete="location">
                    @if(isset($formLocations) && is_array($formLocations))
                    @foreach ($formLocations as $key => $val)
                      <option value={{ $key }}>{{ $val }}</option>
                    @endforeach
                    @endif
                  </select>
                </div>
              </th>
              <th class="w-15" scope="col">
                <div class="input-group">
                  <input id="datePicker" type="text" class="form-control" name="inputDate" data-provide="datepicker" data-date-orientation="bottom" data-date-format="yyyy/mm/dd">
                  <span class="input-group-append">
                    <div class="input-group-text bg-transparent"><i class="fa fa-fw fa fa-calendar"></i></div>
                  </span>
                </div>
              </th>
              <th class="w-15 center" scope="col">
                <span><i class="fa fa-fw fa-clock-o"></i></span>Time
              </th>
              <th class="w-15" scope="col">
                <div>
                  <select class="form-control" id="inputActivity" name="inputActivity">
                    @if(isset($formActivity) && is_array($formActivity))
                    @foreach ($formActivity as $key => $val)
                      <option value={{ $key }}>{{ $val }}</option>
                    @endforeach
                    @endif
                  </select>
                </div>
              </th>
              <th scope="col">
                  <button class="btn btn-outline-secondary" type="button">
                      <i class="fa fa-search"></i>
                  </button></th>
            </form>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="w-30">Dai So</td>
            <td class="w-25 center">Laguna</td>
            <td class="w-15 center">2018/07/02</td>
            <td class="w-15 center">8:40AM</td>
            <td class="w-15" style="color:green;">Time In</td>
            <td></td>
          </tr>
        </tbody>
      </table>
      <div class="container-fluid paginator d-flex">
        <button class="btn btn-outline-dark btn-prev mr-auto"> < Previous </button>
        <button class="btn btn-outline-dark btn-next ml-auto"> > Next </button>
      </div>
    </div>
  </div>
@endsection