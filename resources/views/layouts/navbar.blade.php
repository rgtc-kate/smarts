<!-- Navigation Bar -->
  <nav class="navbar navbar-expand-lg sticky-top smarts-navbar">
    <!-- Navigation Toggler -->
    <div class="navbar-toggler-right">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>

    <!-- Account Navigation -->
    <div class="collapse navbar-collapse flex-column top-nav" id="navbar">
      <ul class="nav navbar-nav px-3 ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-user-circle"></i>
            <span class="nav-link-text">Hi, {{ Auth::user()->first_name." ".Auth::user()->last_name }}</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Manage Account</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log Out</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </div>
       </li>
      </ul>
    </div>

    <!-- Logo -->
    <div class="container d-flex flex-column justify-content-center">
      <span class="h1 mx-auto">S.M.<i class="fa fa-fw fa-user-secret"></i>.R.T.S</span>
      <span class="h6 mx-auto">San Miguel Attendance Record Tracking System</span>
    </div>

    </br>

    <!-- Page Navigation -->
    <div class="container-fluid justify-content-center mid-nav">
      <div class="row w-100">
        <ul class="nav nav-pills w-100 justify-content-center px-3">
          <li class="nav-item {{ strpos(Route::currentRouteName(), 'index') !== false ? 'active' : '' }}">
            <a class="nav-link {{ strpos(Route::currentRouteName(), 'index') !== false ? 'active' : '' }}" href="/">
              <span><i class="fa fa-fw fa-list"></i> Realtime List</span>
            </a>
          </li>
          <li class="nav-item {{ strpos(Route::currentRouteName(), 'product') !== false ? 'active' : '' }}">
            <a class="nav-link {{ strpos(Route::currentRouteName(), 'product') !== false ? 'active' : '' }}" href="/product">
              <span><i class="fa fa-fw fa-cart-plus"></i> Product</span>
            </a>
          </li>
          <li class="nav-item {{ strpos(Route::currentRouteName(), 'plantLocation') !== false ? 'active' : '' }}">
            <a class="nav-link {{ strpos(Route::currentRouteName(), 'plantLocation') !== false ? 'active' : '' }}" href="/location">
              <span><i class="fa fa-fw fa-map-marker"></i> Location</span>
            </a>
          </li>
          <li class="nav-item {{ strpos(Route::currentRouteName(), 'userManage') !== false ? 'active' : '' }}">
            <a class="nav-link {{ strpos(Route::currentRouteName(), 'userManage') !== false ? 'active' : '' }}" href="/user-manage">
              <span><i class="fa fa-fw fa-group"></i> Manage</span>
            </a>
          </li>
          <li class="nav-item {{ strpos(Route::currentRouteName(), 'appli') !== false ? 'active' : '' }}">
            <a class="nav-link {{ strpos(Route::currentRouteName(), 'appli') !== false ? 'active' : '' }}" href="/applications">
              <span><i class="fa fa-fw fa-file"></i> Applications</span>
            </a>
          </li>
          <li class="nav-item {{ strpos(Route::currentRouteName(), 'inspect') !== false ? 'active' : '' }}">
            <a class="nav-link {{ strpos(Route::currentRouteName(), 'inspect') !== false ? 'active' : '' }}" href="/inspections">
              <span><i class="fa fa-fw fa-search"></i> Inspection</span>
            </a>
          </li>
        </ul>
        <!-- .mid-nav -->
      </div>
    </div>
    <hr class="nav-hr">
  </nav>
