@extends('layouts.app')

@section('content')

  
  <!-- Dynamic Content -->
  <div class="container-fluid"> 
    <div class="tab-content clearfix">
      <div class="container-fluid h3">Product List</div>
      <hr class="content-hr">
      <div class="container-fluid">
        <div class="row">
          <button class="btn btn-light ml-auto h6" type="button" data-toggle="modal" data-target="#addProductModal"><i class="fa fa-plus"></i> ADD NEW PRODUCT</button>
        </div>
      </div>
      <table class="table table-responsive table-hover table-border-outline">
        <thead>
          <tr>
            <form method="POST">
              <th class="w-40" scope="col">
                <div class="input-group">
                  <input class="form-control py-2 border-right-0 border" name="productCode"  type="text" placeholder="Product Code">
                </div>
              </th>
              <th class="w-40" scope="col">
                <div class="input-group">
                  <input class="form-control py-2 border-right-0 border" name="productName"  type="text" placeholder="Product Name">
                </div>
              </th>
              <th class="w-20 center" scope="col">Action</th>
              <th class="w-1">
                <button class="btn btn-outline-secondary" type="button">
                    <i class="fa fa-search"></i>
                </button>
              </th>
            </form>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="w-45">PP 320</td>
            <td class="w-45">Product 1</td>
            <td class="w-5 center">
            	<button class="btn btn-outline-dark" data-toggle="modal" data-target="#editProductModal"><span><i class="fa fa-edit"></i></span></button>
            	<button class="btn btn-outline-dark" data-toggle="modal" data-target="#deleteProductModal"><span><i class="fa fa-trash"></i></span></button>
            </td>
            <td></td>
          </tr>
        </tbody>
      </table>
      <div class="container-fluid paginator d-flex">
        <button class="btn btn-outline-dark btn-prev mr-auto"> < Previous </button>
        <button class="btn btn-outline-dark btn-next ml-auto"> > Next </button>
      </div>
    </div>
  </div>

@include('product.modals')
@endsection