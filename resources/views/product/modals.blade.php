
<!-- Modals -->

<!-- Add Product Modal -->
<div id="addProductModal" class="modal fade" tabindex="-1" aria-labelledby="addModalLabel" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header smarts-modal-header">
        <h3 class="modal-title" id="addModalLabel"><span><i class="fa fa-cart-plus"></i></span> Add Product</h3>
      </div>
      <div class="modal-body">
        <form method="POST">
          <div class="form-group row">
            <label for="productCode" class="col-sm-4 col-form-label font-weight-bold">Product Code</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="productCode" name="inputProductCode">
            </div>
          </div>
          <div class="form-group row">
            <label for="productName" class="col-sm-4 col-form-label font-weight-bold">Product Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="productName" name="inputProductName">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="button" class="btn btn-light w-40 font-weight-bold" data-dismiss="modal">ADD</button>
      </div>
    </div>
  </div>
</div>

<!-- Edit Product Modal -->
<div id="editProductModal" class="modal fade" tabindex="-1" aria-labelledby="editModalLabel" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header smarts-modal-header">
        <h3 class="modal-title" id="editModalLabel"><span><i class="fa fa-edit"></i></span> Edit Product</h3>
      </div>
      <div class="modal-body">
        <form method="POST">
          <div class="form-group row">
            <label for="productCode" class="col-sm-4 col-form-label font-weight-bold">Product Code</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="productCode" name="inputProductCode">
            </div>
          </div>
          <div class="form-group row">
            <label for="productName" class="col-sm-4 col-form-label font-weight-bold">Product Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="productName" name="inputProductName">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="button" class="btn btn-light w-40 font-weight-bold" data-dismiss="modal">EDIT</button>
      </div>
    </div>
  </div>
</div>

<!-- Delete Product Modal -->
<div id="deleteProductModal" class="modal fade" tabindex="-1" aria-labelledby="deleteModalLabel" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p class="font-weight-bold">Are you sure you want to delete this data?</p>
      </div>
      <div class="modal-footer smarts-modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Yes</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>