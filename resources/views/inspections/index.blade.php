@extends('layouts.app')
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/inspections.css') }}">
@endpush

@section('content')

  
  <!-- Dynamic Content -->
  <div class="container-fluid"> 
    <div class="tab-content clearfix">
      <div class="container-fluid h3">Inspections Reports</div>
      <hr class="content-hr">
    </div>
    <div>
    </div>

    <div class="row d-flex justify-content-end">
    	<form class="form-inline">
    		<div class="row">
    			<div class="col input-group">
					<div class="input-group-prepend">
						<div class="input-group-text" id="basic-addon1">
							<span><i class="fa fa-map-marker"></i> <i class="fa fa-caret-down"></i></span>
						</div>
					</div>
					<select class="form-control" id="inputLocation" name="inputLocation">
					@if(isset($formLocations) && is_array($formLocations))
					@foreach ($formLocations as $key => $val)
					  <option value={{ $key }} {{ $key == 1 ? 'selected' : ''}}>{{ $val }}</option>
					@endforeach
					@endif
					</select>
    			</div>
    			<div class="col input-group">
					<div class="input-group-prepend">
						<span class="input-group-text" id="basic-addon2">Date</span>
					</div>
					<input type="text" class="form-control" aria-label="Date" placeholder="2017/07/02" aria-describedby="basic-addon2" data-provide="datepicker" data-date-orientation="bottom" data-date-format="yyyy/mm/dd">
					<div class="input-group-append">
						<span class="input-group-text"><i class="fa fa-calendar"></i></span>
					</div>
    			</div>
    			<div class="col-2">
                  <button class="btn btn-outline-secondary" type="button">
                    <i class="fa fa-search"></i>
                  </button>
    			</div>
    		</div>
    	</form>
    </div>
  </div>
@endsection
