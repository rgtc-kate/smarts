@extends('layouts.app')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/login.css') }}">
@endpush

@section('content')
<div class="vertical-center">
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="col-sm-6">
        <div class="d-flex justify-content-center">
          <img class="login-img img-thumbnail img-responsive img-fluid" src="http://www.gstatic.com/webp/gallery/4.jpg">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="row d-flex justify-content-center">
          <div class="login-card card col-sm-8">
            <div class="card-body">
              <form class="form-signin" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                  <input type="text" id="username" name="username" class="form-control form-control-lg" placeholder="Username" value="{{ old('username') }}" required autofocus>

                  @if ($errors->has('username'))
                  <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                  <input type="password" id="password" name="password" class="form-control form-control-lg" placeholder="Password" required>
                  @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
              </form>
            </div>
          </div>
        </div>
      </div>  
    </div>
  </div>
</div>
@endsection
