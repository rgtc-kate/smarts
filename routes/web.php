<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('index');
Route::get('/product', 'ProductController@index')->name('product');
Route::get('/location', 'LocationController@index')->name('plantLocation');
Route::get('/inspections', 'InspectionsController@index')->name('inspections');

// Applications
Route::get('/applications', 'ApplicationsController@index')->name('applications');
Route::get('/applications/view', 'ApplicationsController@view')->name('applications.view');
Route::get('/applications/edit', 'ApplicationsController@edit')->name('applications.edit');

Route::post('/applications/view', 'ApplicationsController@view');
Route::post('/applications/edit', 'ApplicationsController@edit');

// User Management
Route::get('/user-manage', 'UserManageController@index')->name('userManage');
Route::get('/user-manage/add', 'UserManageController@create')->name('userManage.add');
// User Management View
Route::get('/user-manage/personal/view', 'UserManageController@personal')->name('userManage.personal.view');
Route::get('/user-manage/location/view', 'UserManageController@location')->name('userManage.location.view');
Route::get('/user-manage/work-schedule/view', 'UserManageController@workSchedule')->name('userManage.workSchedule.view');
// User Management Edit
Route::get('/user-manage/personal/edit', 'UserManageController@personal')->name('userManage.personal.edit');
Route::get('/user-manage/location/edit', 'UserManageController@location')->name('userManage.location.edit');
Route::get('/user-manage/work-schedule/edit', 'UserManageController@workSchedule')->name('userManage.workSchedule.edit');

Route::post('/user-manage/add', 'UserManageController@create');
Route::post('/user-manage/edit', 'UserManageController@edit');