<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApplicationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applicationTypes = [
            0 => 'All',
            1 => 'Timecard',
            2 => 'Undertime',
            3 => 'Sick Leave',
            4 => 'Vacation Leave',
        ];

        $viewVars = [
            'formAppliTypes' => $applicationTypes
        ];
        return view('applications.index', $viewVars);
    }

    public function view() 
    {
        $applicationTypes = [
            0 => 'All',
            1 => 'Timecard',
            2 => 'Undertime',
            3 => 'Sick Leave',
            4 => 'Vacation Leave',
        ];

        $viewVars = [
            'formAppliTypes' => $applicationTypes
        ];
        return view('applications.view', $viewVars);
    }

    public function edit()
    {
        $applicationTypes = [
            0 => 'All',
            1 => 'Timecard',
            2 => 'Undertime',
            3 => 'Sick Leave',
            4 => 'Vacation Leave',
        ];

        $viewVars = [
            'formAppliTypes' => $applicationTypes
        ];
        return view('applications.edit', $viewVars);
    }
}
