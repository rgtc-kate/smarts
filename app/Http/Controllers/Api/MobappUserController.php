<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Uuid;

class MobappUserController extends Controller
{
	public $successStatus = 200;
	
	/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login()
    {
        if(Auth::attempt(['username' => request('username'), 'password' => request('password')])) {
            $user = Auth::user();

            // - create new api token
            if (is_null($user->api_token)) {
                $token = (string) Uuid::generate();
                
                // - update user object
                $user = User::find($user->id);
                $user->api_token = $token;
                $user->save();
                Auth::setUser($user);                
                $user = Auth::user();
            }

            $success = [
                'token' => $user->api_token,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name
            ];
            return response()->json($success, $this->successStatus); 
        } else {
            return response()->json(['error'=>'Unauthorised'], 401); 
        }
    }

    public function details() {
        $user = User::validateApiToken(request('token'));
        if (isset($user)) {
            return response()->json($user);
        } else {
            return response()->json(['error'=>'Unauthorised'], 401); 
        }
    }

}
