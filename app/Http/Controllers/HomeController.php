<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locationVals = [
            0 => 'Location',
            1 => 'Laguna',
            2 => 'Mandaluyong City',
            3 => 'Pasig City',
            4 => 'Pandacan Manila',
        ];

        $actions = [
            0 => 'Any',
            1 => 'Time In',
            2 => 'Time Out',
        ];

        $viewVars = [
            'formLocations' => $locationVals,
            'formActivity' => $actions
        ];

        return view('list.index', $viewVars);
    }
}
