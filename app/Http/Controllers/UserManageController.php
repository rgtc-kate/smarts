<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class UserManageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locationVals = [
            0 => 'Location',
            1 => 'Laguna',
            2 => 'Mandaluyong City',
            3 => 'Pasig City',
            4 => 'Pandacan Manila',
        ];

        $viewVars = [
            'formLocations' => $locationVals
        ];
        return view('userManage.index', $viewVars);
    }

    public function create()
    {
        $roles = [
            0 => 'Select',
            1 => 'SQ Acceptor',
            2 => 'Quality Assurance',
            3 => 'Floor Manager',
            4 => 'Supervisor',
        ];

        $viewVars = [
            'formRoles' => $roles
        ];

        return view('userManage.add', $viewVars);
    }

    public function personal()
    {
        $roles = [
            0 => 'Select',
            1 => 'SQ Acceptor',
            2 => 'Quality Assurance',
            3 => 'Floor Manager',
            4 => 'Supervisor',
        ];

        $editScreen = strpos(Route::currentRouteName(), 'edit') !== false ? true : false; 

        $viewVars = [
            'formRoles' => $roles,
            'editScreen' => $editScreen
        ];

        return view('userManage.personal', $viewVars);
    }

    public function location()
    {
        $locationVals = [
            0 => 'Select',
            1 => 'Laguna',
            2 => 'Mandaluyong City',
            3 => 'Pasig City',
            4 => 'Pandacan Manila',
        ];

        $editScreen = strpos(Route::currentRouteName(), 'edit') !== false ? true : false; 

        $viewVars = [
            'formLocations' => $locationVals,
            'editScreen' => $editScreen
        ];
        return view('userManage.location', $viewVars);
    }

    public function workSchedule()
    {
        $editScreen = strpos(Route::currentRouteName(), 'edit') !== false ? true : false; 
        
        $viewVars = [
            'editScreen' => $editScreen
        ];
        return view('userManage.workSchedule', $viewVars);
    }

    public function edit()
    {
        $roles = [
            0 => 'Select',
            1 => 'SQ Acceptor',
            2 => 'Quality Assurance',
            3 => 'Floor Manager',
            4 => 'Supervisor',
        ];

        $viewVars = [
            'formRoles' => $roles
        ];

        return view('userManage.edit', $viewVars);
    }
}
